#include <stdio.h>
#include <stdlib.h>
#include <openssl/blowfish.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

struct bf_args {
    size_t outsize;
    size_t insize;
    BF_KEY * key;
    unsigned char * in;
    unsigned char * out;
    unsigned char * ivec;
    int mode;
    int padded;
    long length;
};

void usage();
void bf_encrypt(struct bf_args *);
void bf_decrypt(struct bf_args *);
int readfile(unsigned char *, int, int);

#define READFILE_CHUNK 8 //bytes
#define CBC_MODE 1
#define ECB_MODE 2

void usage() {
  fprintf(stderr, "%s\n", "usage: ./szyfruj cbc|ebc encrypt|decrypt plik_zrodlowy");
  exit(1);
}

int main(int argc, char * argv[]) {
  unsigned char * buf = malloc(READFILE_CHUNK);
  unsigned char * out = malloc(READFILE_CHUNK);
  int fd, outsize, nread;
  char cbc_arg[] = "cbc";
  char ebc_arg[] = "ecb";
  char encrypt_arg[] = "encrypt";
  char decrypt_arg[] = "decrypt";

  struct bf_args args;
  args.out = out;
  args.padded = 0;

  // Zero ivec
  unsigned char * ivec = malloc(8);
  memset(ivec, 0, 8);
  args.ivec = ivec;

  if ( argc != 3 && argc != 4 ) {
    usage();
  }

  if (strcmp(argv[1], cbc_arg) == 0) {
    args.mode = CBC_MODE;
    args.length = READFILE_CHUNK;
  } else if (strcmp(argv[1], ebc_arg) == 0) {
    args.mode = ECB_MODE;
  } else {
    fprintf(stderr, "%s\n", "Wrong mode! Accepted modes: cbc, ecb");
    usage();
  }

  // Tworzenie klucza
  BF_KEY *key = malloc(sizeof(BF_KEY));
  unsigned char * crypt_key = (unsigned char *) "cryptkey";
  int key_len = strlen((const char *) crypt_key);
  BF_set_key(key, key_len, crypt_key);
  args.key = key;

  // Otwarcie pliku
  fd = open(argv[3], O_RDONLY);
  if (fd == -1) {
    fprintf(stderr, "Couldn't open file '%s' \n", argv[3]);
    exit(1);
  }

  // Szyfrowanie
  if (strcmp(argv[2], encrypt_arg) == 0) {

    while(1) {
      nread = readfile(buf, READFILE_CHUNK, fd);
      args.in = buf;

      if (nread == 0) {
        // If input % 8 = 0 then no padding was added by bf_encrypt function.
        // To comply with ANSI padding we add 0 0 0 0 0 0 0 8 padding.
        if (args.padded == 0) {
          memset(buf, '\0', 7);
          memset(buf+7, 8, 1);
          args.insize = 8;
          args.outsize = 8;
          bf_encrypt(&args);
          write(1, args.out, outsize);
        }
        break;
      }

      outsize = (nread + 7) & (~7);
      args.outsize = outsize;
      args.insize = nread;
      bf_encrypt(&args);
      write(1, args.out, outsize);
    }
  }

  //Deszyfrowanie
  if (strcmp(argv[2], decrypt_arg) == 0) {

    nread = readfile(buf, READFILE_CHUNK, fd);
    args.in = buf;
    args.insize = nread;
    bf_decrypt(&args);

    while(1) {
      nread = readfile(buf, READFILE_CHUNK, fd);
      args.in = buf;
      args.insize = nread;

      if (nread == 0) {
        bf_decrypt(&args);
        write(1, args.out, READFILE_CHUNK - args.out[READFILE_CHUNK-1]);
        break;
      } else {
        write(1, args.out, READFILE_CHUNK);
        bf_decrypt(&args);
      }

    }
  }

  free(out);
  free(key);
  free(buf);
  free(ivec);
}

void bf_decrypt(struct bf_args * args) {
  unsigned char * decrypted_text = malloc(READFILE_CHUNK);
  unsigned char * outnext = decrypted_text;

  while(args->insize) {
    if (args->mode == ECB_MODE) {
      BF_ecb_encrypt(args->in, outnext, args->key, BF_DECRYPT);
    } else if (args ->mode == CBC_MODE) {
      BF_cbc_encrypt(args->in, outnext, args->length, args->key, args->ivec, BF_DECRYPT);
    }
    args->in += 8;
    outnext += 8;
    args->insize -= 8;
  }

  memcpy(args->out, decrypted_text, READFILE_CHUNK);
  free(decrypted_text);
}

void bf_encrypt(struct bf_args * args) {
  unsigned char * outnext = args->out;
  unsigned char buf[8];
  int bytes_to_ignore = 0, i;

  while (args->insize >= 8) {
    if (args->mode == ECB_MODE) {
      BF_ecb_encrypt(args->in, outnext, args->key, BF_ENCRYPT);
    } else if (args->mode == CBC_MODE) {
      BF_cbc_encrypt(args->in, outnext, args->length, args->key, args->ivec, BF_ENCRYPT);
    }
    args->in += 8;
    outnext += 8;
    args->insize -= 8;
  }

  // add padding
  if ( args->insize > 0 ) {
    bytes_to_ignore = 8 - args->insize;

    memcpy(buf, args->in, args->insize);
    for (i = args->insize; i<7; i++) {
      buf[i] = '\0';
    }

    memset(buf+7, bytes_to_ignore, 1);
    if (args->mode == ECB_MODE) {
      BF_ecb_encrypt(buf, outnext, args->key, BF_ENCRYPT);
    } else if (args->mode == CBC_MODE) {
      BF_cbc_encrypt(buf, outnext, args->length, args->key, args->ivec, BF_ENCRYPT);
    }
    args->padded = 1;
  }
}

int readfile(unsigned char * buf, int bufsiz, int fd) {
  int nread;

  nread = read(fd, buf, bufsiz);
  if (nread == -1) {
    perror("Read error.");
    exit(1);
  }
  return nread;
}
