#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char * argv[]) {
	int count[26] = {0};
	char c;
	FILE * source, * dest;

	if (argc != 2) {
		printf("Zla liczba argumentow\n");
		printf("Uzycie: %s plik_zdrolowy\n", argv[0]);
   		return 1;
	}

	source = fopen(argv[1], "r");
	dest = fopen("occurances.csv", "w");

	if ( source == NULL ) {
		perror("[-] Nie udalo sie otworzyc pliku \"tekst\"");
		return 1;
	}

	if ( dest == NULL ) {
		perror("[-] Nie udalo sie otworzyc pliku \"tekst\"");
		return 1;
	}

	printf("[+] Zliczam wystapienia konkretnych znakow... \n");
	while ((c = fgetc(source)) != EOF) {
		if (c >= 'A' && c <= 'z') {
			/* Konwertuj duze znaki na male */
			if ( c >= 'A' && c <= 'Z' ) {
				c = c + 32;
			}
			count[c - 'a']++;
		}
	}

	for (int i = 0; i < 26; i++) {
		c = i + 'a';
		fprintf(dest, "%c", c);
		if ( i != 25 )
			fprintf(dest, "%c", ',');
	}

	fprintf(dest, "%c", '\n');

	for (int i = 0; i < 26; i++) {
		fprintf(dest, "%i", count[i]);
		if ( i != 25 )
			fprintf(dest, "%c", ',');
	}

	for (int i = 0; i < 26; i++) {
		c = i + 'a';
		printf("%c=%i,", c, count[i]);
	}


	printf("[+] Znaki zliczone \n");
	fclose(source);
	fclose(dest);
	return 0;
}
