#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
	char c;
	FILE * source, * dest;

	if (argc != 2) {
		printf("Uzycie: ./cezar przesuniecie");
		return 1;
	}

	signed int shift = atoi(argv[1]);
	printf("[+] Przesuniecie wynosi: %i \n", shift);

	source = fopen("tekst", "r");
	dest = fopen("dest", "w");

	if ( source == NULL ) {
		perror("[-] Nie udalo sie otworzyc pliku \"tekst\"");
		return 1;
	}

	if ( dest == NULL ) {
		perror("[-] Nie udalo sie otworzyc pliku \"dest\"");
		return 1;
	}

	printf("[+] Szyfruje plik... \n");
	if (shift >= 0) {
		while ((c = fgetc(source)) != EOF) {
			if ( c >= 'A' && c <= 'Z' ) {
				c = c + 32;
			}

			if ( c == ' ' ) {
				continue;
			}

			if ( c + shift <= 'z' ) {
				c = c + shift;
			} else {
				c = c + shift - 26;
			}
			printf("%c", c);
			fwrite(&c, 1, 1, dest);
		}
	} else {
		while ((c = fgetc(source)) != EOF) {
			if ( c >= 'A' && c <= 'Z' ) {
				c = c + 32;
			}

			if ( c == ' ' ) {
				continue;
			}

			if ( c + shift >= 'a' ) {
				c = c + shift;
			} else {
				c = c + shift + 26;
			}
			printf("%c", c);
			fwrite(&c, 1, 1, dest);
		}
	}


	printf("[+] Plik zostal zaszyfrowany \n");
	fclose(source);
	fclose(dest);
	return 0;
}
